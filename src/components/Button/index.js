import classNames from "classnames/bind";
import styles from "./Button.module.scss";
import { Link } from "react-router-dom";

const cx=classNames.bind(styles);
function Button({to, href, onClick,
    primary=false,outline=false,small=false,large=false, simple=false, round=false,
    children,...passProps}) {
    //Comp mặc định là button
    let Comp='button';
    const classes=cx('wrapper',{
        primary,
        outline,
        small,
        large,
        simple,
        round,
    });
    const props={
        //Mặc định luôn có sự kiện onClick
        onClick,
        ...passProps,
    };
    if(to){
        //push to vào object
        props.to=to;
        //Comp là link của router
        Comp=Link;
    }else if(href){
        props.href=href;
        //Comp là thẻ a
        Comp='a';
    }
    return ( 
        <Comp className={classes} {...props}>
            <span>{children}</span>
        </Comp>
     );
}

export default Button;