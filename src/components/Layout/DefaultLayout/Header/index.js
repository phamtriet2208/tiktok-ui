import classNames from 'classnames/bind';
import styles from './Header.module.scss';
import images from '~/assets/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faA, faCircleQuestion, faEllipsisVertical, faKeyboard, faPlus } from '@fortawesome/free-solid-svg-icons';

import Button from '~/components/Button';
import Tippy from '@tippyjs/react/headless';
import { useState } from 'react';

import { PopRight } from './PopperRight';
import OptionComp from './PopperRight/Component/Option';
import Search from './Search';

const cx = classNames.bind(styles);


function Header() {
    return (
        <header className={cx('wrapper')}>
            <div className={cx('inner')}>
                <div className={cx('logo')}>
                    <img src={images.logo} alt="Tiktok" />
                </div>
                <Search />
                <div className={cx('actions')}>
                    <Button
                        simple
                    >
                        <FontAwesomeIcon icon={faPlus} /> Tải lên
                    </Button>
                    <Button
                        primary
                    >Đăng nhập</Button>

                    <Tippy
                        interactive
                         render={attrs => (
                            <div className={cx('search-result')} tabIndex="-1" {...attrs}>
                                <PopRight>
                                    <OptionComp
                                        icon={<FontAwesomeIcon icon={faA}/>}
                                        text="Tiếng Việt"
                                    />
                                    <OptionComp
                                        icon={<FontAwesomeIcon icon={faCircleQuestion}/>}
                                        text="Phản hồi và trợ giúp"
                                     />
                                     <OptionComp
                                        icon={<FontAwesomeIcon icon={faKeyboard}/>}
                                        text="Phím tắt trên bàn phím"
                                     />
                                </PopRight>
                            </div>
                        )}
                    >
                        <button className={cx('btnTripleDotV')}>
                            <FontAwesomeIcon icon={faEllipsisVertical}></FontAwesomeIcon>
                        </button>
                    </Tippy>
                    

                </div>
            </div>
        </header>);
}

export default Header;