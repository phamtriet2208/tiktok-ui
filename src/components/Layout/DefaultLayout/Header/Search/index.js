import classNames from 'classnames/bind';
import styles from './Search.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useRef, useState, useEffect } from 'react';
import { Wrapper as PopperWrapper } from '~/components/Layout/DefaultLayout/Header/Popper';
import AccountItem from '~/components/Layout/DefaultLayout/Header/Component/AccountItem';
import { faCircleXmark, faMagnifyingGlass, faSpinner } from '@fortawesome/free-solid-svg-icons';
import Tippy from '@tippyjs/react/headless';

const cx = classNames.bind(styles);
function Search() {
    //Tạo 1 mảng kết quả tìm kiếm
    const [searchResult, setSearchResult] = useState([]);
    const [searchValue, setSearchValue] = useState('');
    const [showResult, setShowResult] = useState(true);
    const [showLoading, setShowLoading] = useState(false);
    const inputRef = useRef();
    useEffect(() => {
        if (!searchValue) return;
        setShowLoading(true);
        //fetch API, sau đó đổi kết quả trả về dưới dạng JSON, sau đó...
        fetch(`https://tiktok.fullstack.edu.vn/api/users/search?q=${encodeURIComponent(searchValue)}&type=less`)
            .then(response => response.json())
            .then(response => {
                setSearchResult(response.data);
                setShowLoading(false);
            }).catch(() => {
                setShowLoading(false);
            })
    }, [searchValue]);
    const handleHideResult = () => {
        setShowResult(false);
    }
    const handleClear = () => {
        setSearchValue('');
        setSearchResult([]);
    }
    const handleChangeValue=()=>{
        setSearchValue(inputRef.current.value);
    }
    return (
        <Tippy
            //cho active vào kết quả
            interactive
            //Tippy chỉ hiện khi mảng tìm kiếm có kết quả
            visible={searchValue && showResult && searchResult.length > 0}
            onClickOutside={handleHideResult}
            render={attrs => (
                <div className={cx('search-result')} tabIndex="-1" {...attrs}>
                    <PopperWrapper>
                        <h4 className={cx('search-title')}>
                            Accounts
                        </h4>
                        {
                            searchResult && searchResult.map((item) => (
                                <AccountItem key={item.id} data={item} />
                            ))
                        }
                    </PopperWrapper>
                </div>
            )}
        >
            <div className={cx('search')}>
                <input placeholder='Search accounts and video' spellCheck="false"
                    ref={inputRef}
                    value={searchValue}
                    onFocus={() => setShowResult(true)}
                    onChange={handleChangeValue}
                />
                {searchValue && !showLoading && <button
                    className={cx('clear')}
                    onClick={handleClear}
                ><FontAwesomeIcon icon={faCircleXmark} /></button>}
                {showLoading && <button className={cx('loading')} ><FontAwesomeIcon icon={faSpinner} /></button>}
                
                <button className={cx('search-btn')}><FontAwesomeIcon icon={faMagnifyingGlass} /></button>
            </div>
        </Tippy>
    );
}

export default Search;