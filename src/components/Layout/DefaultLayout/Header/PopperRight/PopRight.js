import classNames from "classnames/bind";
import  styles from "./PopRight.module.scss";

const cx=classNames.bind(styles);

function PopRight({children}) {
    return ( 
        <div className={cx('popRight')}>
            {children}
        </div>
     );
}

export default PopRight;