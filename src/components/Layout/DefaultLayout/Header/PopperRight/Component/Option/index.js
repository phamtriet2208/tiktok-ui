import styles from './Option.module.scss';
import classNames from 'classnames/bind';

const cx=classNames.bind(styles);
function OptionComp({icon,text}) {
    return ( 
        <div className={cx('container')}>
            <div className={cx('wrapper')}>
                <span className={cx('icon')}>{icon}</span>
                <span className={cx('text')}>{text}</span>
            </div>
        </div>
     );
}

export default OptionComp;