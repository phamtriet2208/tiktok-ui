import styles from './Slidebar.module.scss';
import classNames from 'classnames/bind';
import Category from './Component/Category';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHouse, faUserGroup, faVideo } from '@fortawesome/free-solid-svg-icons';
import Button from '~/components/Button';
import AccountItem from '../Header/Component/AccountItem';

const cx = classNames.bind(styles);
function Slidebar() {
    return (
        <aside className={cx('wrapper')}>
            <Category
                icon={<FontAwesomeIcon icon={faHouse} />}
                text="Dành cho bạn"
                active
            />
            <Category
                icon={<FontAwesomeIcon icon={faUserGroup} />}
                text="Đang Follow"
            />
            <Category
                icon={<FontAwesomeIcon icon={faVideo} />}
                text="LIVE"
            />
            <div className={cx('middle')}>
                <h6 className={cx('content')}>Đăng nhập để follow các tác giả, thích video và xem bình luận.</h6>
                <Button
                    outline
                    large
                >Đăng nhập</Button>
            </div>
            <div className={cx('recommendedAccount')}>
                <h6 className={cx('title')}>Tài khoản được đề xuất</h6>
                <AccountItem />
                <AccountItem />
                <AccountItem />
                <h6 className={cx('viewAll')}>Xem tất cả</h6 >
            </div>
        </aside>
    );
}

export default Slidebar;