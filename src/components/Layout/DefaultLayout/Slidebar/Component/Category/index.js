import styles from "./Category.module.scss";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);
function Category({icon,text,active=false}) {
    const classes=cx({
        icon,
        text,
        active,
    });
    return (
        <div className={cx('wrapper')}>
            <span className={classes}>{icon}</span>
            <span className={classes}>{text}</span>
        </div>
    );
}

export default Category;