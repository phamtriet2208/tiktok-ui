import Home from '~/pages/Home';
import Following from '~/pages/Following';
import Profile from '~/pages/Profile'; 
import Upload from '~/pages/Upload'; 
import UploadLayout from '~/components/Layout/UploadLayout';

//Router public
const publicRoutes=[
    {path:"/",component:Home},
    {path:"/following",component:Following},
    {path:"/@:nickname",component:Profile},
    {path:"/upload",component:Upload,layout:UploadLayout},

];

//Router cho login
const privateRoutes=[
    
];
export {publicRoutes,privateRoutes};