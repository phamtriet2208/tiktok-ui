import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { publicRoutes } from '~/routes';
import { DefaultLayout } from './components/Layout';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          {publicRoutes.map((route, index) => {
            // Nếu layout không tồn tại thì lấy DefaultLayout
            const Layout = route.layout || DefaultLayout;
            // Muốn dùng Component trong JSX phải viết hoa chữ cái đầu
            const Page = route.component;
            return <Route key={index} path={route.path} element={
              <Layout>
                {/* Page trở thành children của Layout */}
                <Page />
              </Layout>
              
            } />;
          })}
        </Routes>
      </div>
    </Router>
  );
}

export default App;
